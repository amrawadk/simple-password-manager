from tkinter import ttk
import PySimpleGUI as sg
import CSCI101_Demo_Project as dp
from common import initialize

sg.theme("DarkBlue")
sg.set_options(font=("Courier New", 16))

window_homepage = [
    [sg.Text("Enter your name:"), sg.Input(key="name")],
    [sg.Button("Continue"), sg.Button("Exit")],
]

window_select_action = [
    [sg.Text("Welcome!", justification="center", size=(25, 2), pad=(25, 25))],
    [sg.Text("What do you want to do?")],
    [sg.Button("Add"), sg.Button("Remove"), sg.Button("SwitchUser")],
]

window_add_password = [
    [sg.Text("Site: "), sg.InputText(key="site")],
    [sg.Text("Account Name: "), sg.InputText(key="account_name")],
    [sg.Text("Password: "), sg.InputText(key="password")],
    [sg.Button("Save Password"), sg.Button("Cancel")],
]

tab_group = [
    [sg.Tab("TAB 1", window_homepage, key="WINDOW_HOME")],
    [sg.Tab("TAB 2", window_select_action, key="WINDOW_SELECT_ACTION")],
    [sg.Tab("TAB 2", window_add_password, key="WINDOW_ADD_PASSWORD")],
]
layout = [
    [sg.TabGroup(tab_group, border_width=0, pad=(0, 0), key="TABGROUP")],
]

window = sg.Window("Title", layout, finalize=True)
style = ttk.Style()
style.layout("TNotebook.Tab", [])  # Hide tab bar

# Initialize Script
fer = initialize()


while True:

    event, values = window.read()
    if event in (sg.WINDOW_CLOSED, "Exit"):
        break
    elif event == "Continue":
        window["WINDOW_SELECT_ACTION"].select()
    elif event == "SwitchUser":
        window["WINDOW_HOME"].select()
    elif event == "Cancel":
        window["WINDOW_SELECT_ACTION"].select()
    elif event == "Add":
        window["WINDOW_ADD_PASSWORD"].select()
    elif event == "Save Password":
        dp.add_gui(
            fer=fer,
            user=values["name"],
            site=values["site"],
            account_name=values["account_name"],
            password=values["password"],
        )

window.close()
