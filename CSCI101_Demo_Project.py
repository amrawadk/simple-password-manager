from cryptography.fernet import Fernet
import string
import random
import re


def write_key():
    key = Fernet.generate_key()
    with open("key.key", "wb") as key_file:
        key_file.write(key)

def load_key():
    file = open("key.key", "rb")
    key = file.read()
    file.close()
    return key

def full_view(fer,user):
    with open(f"{user}.txt", "r") as f:
        for line in f.readlines():
            data = line.rstrip()
            site ,user, passw = data.split("|")
            print("Site:", site,"User:", user, "| Password:",
                  fer.decrypt(passw.encode()).decode())

def specific_view(fer,user):
    """
    input: Site , passwords file
    return: the row (User + password)

    """
    sitec = input("Enter the site you want to check: ")

    with open (f"{user}.txt","r") as file:
        rows = file.readlines()

    for row in rows:
        row = row.rstrip()
        site,user,passw = row.split("|")
        if sitec == site:
            passw = fer.decrypt(passw.encode()).decode()
            return user,passw


def check_strength (n):
    """
    input: (Int) of length of generated password

    check if it is within range or not (If not input again)
    """

    while n < 8:
        print(f"Password is too small! Enter a length from 8-20 character")

        n = int(input("Please enter the number again: "))

    while n > 20:
        print(f"Password is too big! Enter a length from 8-20 character")

        n = int(input("Please enter the number again: "))

    return n


def valid_password(pswd):
    """
    Input: password
    Output: Valid or not (Input again if not)
    """
    pattern = r"[A-Za-z0-9@#$%^&+=]{8,20}"
    match = re.fullmatch(pattern,pswd)
    if match:
        return pswd
    else:
        valid_password(input("Password is not valid! Please enter again: "))

def add(fer,user):
    site = input("Site: ")
    name = input('Account Name: ')

    pwd = input("Password: ")

    pwd_checked = valid_password(pwd)

    with open(f"{user}.txt", "a") as f:
        f.write(site + "|" + name + "|" + fer.encrypt(pwd_checked.encode()).decode() + "\n")

def is_valid_password(pswd) -> bool:
    """
    Input: password
    Output: Valid or not (Input again if not)
    """
    pattern = r"[A-Za-z0-9@#$%^&+=]{8,20}"
    match = re.fullmatch(pattern,pswd)
    return bool(match)

def add_gui(fer,user, site, account_name, password):

    if is_valid_password(password):
        with open(f"{user}.txt", "a") as f:
            f.write(site + "|" + account_name + "|" + fer.encrypt(password.encode()).decode() + "\n")
        print("Password saved")
    else:
        print("Password is not valid! Please try again")


def add_generated(fer,user):
    """
    input: up_l . dp_p
    output: Generate and write to user.txt directly
    """
    site = input("Site: ")
    name = input('Account Name: ')

    n = int(input('How many characters in the password: '))
    n = check_strength(n)

    l1=list(string.ascii_lowercase)
    l2=list(string.ascii_uppercase)
    l3=list(string.digits)
    l4=list(string.punctuation)

    random.shuffle(l1) ; random.shuffle(l2) ; random.shuffle(l3) ; random.shuffle(l4)


    up_l=round(n*(30/100))
    dp_p=round(n*(20/100))

    password = []

    for i in range(up_l):
        password.append(l1[i])
        password.append(l2[i])

    for i in range(dp_p):
        password.append(l3[i])
        password.append(l4[i])

    random.shuffle(password)

    pwd = "".join(password)

    user = user
    with open(f"{user}.txt", "a") as f:
        f.write(site + "|" + name + "|" + fer.encrypt(pwd.encode()).decode() + "\n")

def match_pass(fer,user):
    """
    input: Password Input
    Return : return of all the sites
    """
    pswdC = input("Enter the password: ")
    with open(f"{user}.txt" , 'r') as file:
        rows = file.readlines()

    matched_sites = []

    for row in rows:
        row = row.rstrip().split("|")
        pswd = row[2]
        pswd = fer.decrypt(pswd.encode()).decode()
        if pswdC == pswd:
            matched_sites.append(row[0])

    return matched_sites

def remove_pswd(user,site):
    with open(f"{user}.txt","r") as f:
        lines = f.readlines()
    with open(f"{user}.txt","w") as f:
        for line in lines:
            if not line.startswith(site):
                f.write(line.strip("\n"))
                f.write("\n")
            else:
                print(f"Site {site} has been deleted")


# =============================================================================
# new_file = open("sample.txt", "w")
# for line in lines:
#     if line.strip("\n") != "line2":
#         new_file.write(line)
#
# new_file.close()
# =============================================================================

# =============================================================================
#     with open("f{user}.txt", "a") as fr:
#         lines = fr.readlines()
#         for line in lines:
#             if line.find(site) == -1:
#                 pass
#             else:
#                 del line
# =============================================================================

# =============================================================================
#     with open (f"{user}.txt", "a") as f:
#
#         for row in f.readlines():
#             if row.startswith(site):
#                 row.del
# =============================================================================


            # line_s = row.rstrip().split("|")
            # if line_s[0] == site:
            #     del row
