from CSCI101_Demo_Project import *
from getpass import getpass
from common import initialize

fer = initialize()

user_input = input("Enter your user name: ")
# masterpswd = getpass("Enter your master password : ")

while True:

    mode = input(
        "Would you like to add,remove a new password or view existing ones (view, add, remove), press q to quit? ").lower()
    if mode == "q":
        break
    if mode == "view":
        view_mode = input("Enter the view mode (full,specific,matched): ")
        if view_mode == "full":
            full_view(fer,user_input)
        elif view_mode == "specific":
            user,passw = specific_view(fer,user_input)
            print(f"User name is {user} and the password is {passw}")
        elif view_mode == "matched":
            matches = match_pass(fer,user_input)
            matches = " ".join(matches)
            print(f"Your password is  matched with the following sites {matches}")
    
    elif mode == "add":
        add_mode = input("Enter the add mode (manual,auto): ")
        if add_mode == "manual":
            add(fer,user_input)
        elif add_mode == "auto":
            add_generated(fer,user_input)
    elif mode == "remove":
        site_rm = input("Enter the site that you want to delete? ")
        remove_pswd(user_input,site_rm)
        
    else:
        print("Invalid mode.")
        continue


