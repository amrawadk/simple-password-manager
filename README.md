# Setup

We use [poetry](https://python-poetry.org/) for package management, this provides an easier integration with `pip` and virtual environments. To create the development environment, simply install `poetry` ([instructions](https://python-poetry.org/docs/#installation)) and then run `poetry install` to install for the first time.

Whenever you want to run the environment again, just run `poetry shell`.

