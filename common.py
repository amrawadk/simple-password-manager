from cryptography.fernet import Fernet

import CSCI101_Demo_Project as dp
import os

def initialize() -> Fernet:
    """
    Initialize the script by loading the key, returns a Fernet object
    """
    mydir = os.getcwd()
    d = os.listdir(mydir)

    if "key.key" in d:
        pass
    else:
        dp.write_key()
        

    key = dp.load_key()
    return Fernet(key)
